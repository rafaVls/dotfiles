-- prompts user to add executable("x") permissions for the file owner if not
-- already set
local function make_executable(event)
  local filename = event["match"]
  local ls_out = vim.fn.system({ "ls", "-l", filename })

  if ls_out:sub(4, 4) == "x" then
    return
  end

  local choice = vim.fn.input({
    prompt = "Change file permissions to u+x? (y/n): ",
    default = "n",
    cancelreturn = "n",
  })

  if choice == "y" then
    vim.fn.system({ "chmod", "u+x", filename })
  end
end

local group_id = vim.api.nvim_create_augroup("RafaCommands", {})

-- TODO: make a skeleton to add the shebang at the top of .sh files
--vim.api.nvim_create_autocmd("BufNewFile", {
--  pattern = "*.sh",
--  group = group_id,
--  callback = function(_)
--    print("new file!")
--  end
--})

vim.api.nvim_create_autocmd("BufWritePost", {
  pattern = "*.sh",
  group = group_id,
  callback = make_executable
})
