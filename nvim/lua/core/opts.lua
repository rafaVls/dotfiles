vim.opt.nu = true
vim.opt.relativenumber = true

-- https://stackoverflow.com/questions/51995128/setting-autoindentation-to-spaces-in-neovim
vim.opt.tabstop = 2
vim.opt.softtabstop = 0 -- 0 to disable the 'soft tab stops' feature
vim.opt.shiftwidth = 0  -- 0 to replicate the value of 'tabstop'
vim.opt.expandtab = true
vim.opt.smartindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.termguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

vim.opt.colorcolumn = "80"

-- Netrw overrides
vim.g.netrw_bufsettings = "noma nomod nu nobl nowrap ro"
