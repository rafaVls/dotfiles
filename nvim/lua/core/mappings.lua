vim.g.mapleader = " "
vim.keymap.set("n", "<Leader>pv", vim.cmd.Ex)

if string.find(vim.loop.os_uname().release, "WSL") then
  -- because WSL, this way I can access vim's block visual mode
  vim.keymap.set("n", "<leader>V", "<C-v>")
end
