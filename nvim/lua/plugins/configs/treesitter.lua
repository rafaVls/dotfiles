return {
  ensure_installed = {
    "c",
    "cpp",
    "make",
    "lua",
    "bash",
    "javascript",
    "typescript",
    "css",
    "html",
    "json",
    "python",
    "dockerfile",
  },
  highlight = { enable = true },
}
