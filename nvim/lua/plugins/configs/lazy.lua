return {
  defaults = { lazy = true },
  performance = {
    disabled_plugins = {
      "gzip",
      "matchit",
      "tarplugin",
      "tohtml",
      "tutor",
      "zipPlugin"
    },
  },
}
