local dap = require("dap")
local osv = require "osv"

local adapters = {
  ["gdb"] = {
    type = "executable",
    command = "gdb",
    args = { "-i", "dap" },
  },
  ["bashdb"] = {
    type = "executable",
    command = vim.fn.stdpath("data") .. "/mason/packages/bash-debug-adapter/bash-debug-adapter",
    name = "bashdb",
  },
  ["python"] = function(cb, config)
    if config.request == "attach" then
      ---@diagnostic disable-next-line: undefined-field
      local port = (config.connect or config).port
      ---@diagnostic disable-next-line: undefined-field
      local host = (config.connect or config).host or "127.0.0.1"
      cb({
        type = "server",
        port = assert(port, "`connect.port` is required for a python `attach` configuration"),
        host = host,
        options = {
          source_filetype = "python",
        },
      })
    else
      cb({
        type = "executable",
        command = vim.fn.stdpath("data") .. "/mason/packages/debugpy/venv/bin/python",
        args = { "-m", "debugpy.adapter" },
        options = {
          source_filetype = "python",
        },
      })
    end
  end,
  ["nlua"] = function(callback, config)
    callback({
      type = "server",
      host = config.host or "127.0.0.1",
      port = config.port or 8086
    })
  end
}

local configurations = {
  ["c"] = {
    {
      name = "Launch",
      type = "gdb",
      request = "launch",
      program = function()
        return vim.fn.input({
          prompt = "Path to executable: ",
          default = vim.fn.getcwd() .. "/",
          completion = "file"
        })
      end,
      cwd = "${workspaceFolder}",
    },
  },
  ["sh"] = {
    {
      type = "bashdb",
      request = "launch",
      name = "Launch file",
      showDebugOutput = true,
      pathBashdb = vim.fn.stdpath("data") .. "/mason/packages/bash-debug-adapter/extension/bashdb_dir/bashdb",
      pathBashdbLib = vim.fn.stdpath("data") .. "/mason/packages/bash-debug-adapter/extension/bashdb_dir",
      trace = true,
      file = "${file}",
      program = "${file}",
      cwd = "${workspaceFolder}",
      pathCat = "cat",
      pathBash = "/bin/bash",
      pathMkfifo = "mkfifo",
      pathPkill = "pkill",
      args = {},
      env = {},
      terminalKind = "integrated",
    },
  },
  ["python"] = {
    {
      -- The first three options are required by nvim-dap
      type = "python", -- the type here established the link to the adapter definition: `dap.adapters.python`
      request = "launch",
      name = "Launch file",

      -- Options below are for debugpy, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings for supported options

      program = "${file}", -- This configuration will launch the current file if used.
      pythonPath = function()
        -- debugpy supports launching an application with a different interpreter then the one used to launch debugpy itself.
        -- The code below looks for a `venv` or `.venv` folder in the current directly and uses the python within.
        -- You could adapt this - to for example use the `VIRTUAL_ENV` environment variable.
        local cwd = vim.fn.getcwd()
        if vim.fn.executable(cwd .. "/venv/bin/python") == 1 then
          return cwd .. "/venv/bin/python"
        elseif vim.fn.executable(cwd .. "/.venv/bin/python") == 1 then
          return cwd .. "/.venv/bin/python"
        else
          return "/usr/bin/python3"
        end
      end,
    },
  },
  ["lua"] = {
    {
      type = "nlua",
      request = "attach",
      name = "Attach to running Neovim instance",
    }
  },
}

dap.adapters = adapters
dap.configurations = configurations

vim.keymap.set("n", "<F5>", function() dap.continue() end)
vim.keymap.set("n", "<Leader>do", function() dap.step_over() end)
vim.keymap.set("n", "<Leader>di", function() dap.step_into() end)
vim.keymap.set("n", "<Leader>dso", function() dap.step_out() end)
vim.keymap.set("n", "<Leader>dr", function() dap.repl.open() end)
vim.keymap.set("n", "<Leader>b", function() dap.toggle_breakpoint() end)
vim.keymap.set("n", "<Leader>cb", function()
  local condition = vim.fn.input({ prompt = 'Condition for breakboint: "' })
  dap.toggle_breakpoint(condition)
end)

local widgets = require("dap.ui.widgets")
vim.keymap.set("n", "<Leader>df", function()
  widgets.centered_float(widgets.frames)
end)
vim.keymap.set("n", "<Leader>ds", function()
  widgets.sidebar(widgets.scopes).toggle()
end)
vim.keymap.set({ "n", "v" }, "<Leader>dh", function() widgets.hover() end)

-- one-small-step-for-vimkind keymaps
vim.keymap.set("n", "<F6>", function() osv.run_this() end)
