local ls = require("luasnip")
local types = require("luasnip.util.types")

-- tell luasnip where to find snippet files. If nil, searches "rtp"
local snippets_dir = vim.api.nvim_list_runtime_paths()[1] .. "/lua/snippets"
require("luasnip.loaders.from_lua").load({
  paths = { snippets_dir }
})

ls.config.set_config({
  -- with dynamic snippets, it updates as we type
  updateevents = "TextChanged,TextChangedI",
  ext_opts = {
    [types.choiceNode] = {
      active = {
        virt_text = { { "<-", "Selection" } }
      }
    }
  }
})

-- keymaps
-- don't have one for expanding becuase I'm using cmp_luasnip
vim.keymap.set("i", "<C-k>", function()
  if ls.jumpable(1) then
    ls.jump(1)
  end
end, { silent = true })

vim.keymap.set({ "i", "s" }, "<C-j>", function()
  if ls.jumpable(-1) then
    ls.jump(-1)
  end
end, { silent = true })

vim.keymap.set({ "i", "s" }, "<C-E>", function()
  if ls.choice_active then
    ls.change_choice(1)
  end
end, { silent = true })

vim.keymap.set("i", "<C-S>", function()
  require("luasnip.extras.select_choice")()
end)

vim.keymap.set("n", "<leader><leader>s", function()
  require("snippets.nodes")
  require("snippets.c")
  require("snippets.lua")
  require("snippets.lua")

  print("Reloaded all snippet files")
end)
