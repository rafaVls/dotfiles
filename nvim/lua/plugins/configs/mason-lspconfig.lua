local capabilities = require("cmp_nvim_lsp").default_capabilities()

return {
  ensure_installed = {
    "lua_ls",
    "bashls",
    "clangd",
    "cssls",
    "html",
    "tsserver",
  },
  handlers = {
    function(server_name)
      require("lspconfig")[server_name].setup {
        capabilities = capabilities
      }
    end,
  }
}
