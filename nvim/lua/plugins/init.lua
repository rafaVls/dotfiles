local lazy_opts = require("plugins.configs.lazy")

local plugins = {
  {
    "catppuccin/nvim",
    lazy = false,
    priority = 1000,
    opts = function()
      return require("plugins.configs.catppuccin")
    end,
    config = function(_, opts)
      require("catppuccin").setup(opts)
      vim.cmd.colorscheme("catppuccin")
    end,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    event = { "BufReadPost", "BufNewFile" },
    cmd = { "TSInstall", "TSBufenable", "TSBufDisable", "TSModuleInfo" },
    build = ":TSUpdate",
    opts = function()
      return require("plugins.configs.treesitter")
    end,
    config = function(_, opts)
      require("nvim-treesitter.configs").setup(opts)
    end,
  },
  {
    "williamboman/mason-lspconfig.nvim",
    dependencies = {
      "williamboman/mason.nvim",
      "neovim/nvim-lspconfig",
      "folke/neodev.nvim"
    },
    lazy = false,
    opts = function()
      return require("plugins.configs.mason-lspconfig")
    end,
    config = function(_, opts)
      require("mason").setup()
      require("neodev").setup()
      require("mason-lspconfig").setup(opts)
      require("plugins.configs.lspconfig")
    end,
  },
  {
    "hrsh7th/nvim-cmp",
    event = { "BufReadPost", "BufNewFile" },
    dependencies = {
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "saadparwaiz1/cmp_luasnip", -- see snippets in cmp, optional
      "L3MON4D3/LuaSnip",         -- snippets, optional
    },
    config = function()
      require("plugins.configs.cmp")
    end
  },
  {
    "L3MON4D3/LuaSnip",
    version = "v2.*",                -- https://github.com/L3MON4D3/LuaSnip/releases for latest major version
    build = "make install_jsregexp", -- optional
    config = function()
      require("plugins.configs.luasnip")
    end
  },
  {
    "mfussenegger/nvim-dap",
    event = { "BufReadPost", "BufNewFile" },
    dependencies = { "jbyuki/one-small-step-for-vimkind" }, -- for lua debugging
    config = function()
      require("plugins.configs.dap")
    end,
  },
  {
    "nvim-telescope/telescope.nvim",
    event = { "Bufenter" },
    tag = "0.1.5",
    dependencies = { "nvim-lua/plenary.nvim" },
    keys = {
      { "<leader>pf", "<cmd>Telescope find_files<cr>" },
      { "<leader>ph", "<cmd>Telescope help_tags<cr>" },
      { "<leader>ps", "<cmd>Telescope live_grep<cr>" },
      { "<C-p>",      "<cmd>Telescope git_files<cr>" },
    },
  },
  {
    "nvim-lualine/lualine.nvim",
    lazy = false,
    dependencies = { "nvim-tree/nvim-web-devicons" },
    opts = {}, -- will call require("lualine").setup({})
  },
  {
    "windwp/nvim-autopairs",
    event = { "InsertEnter" },
    config = function()
      require("plugins.configs.autopairs")
      require("nvim-autopairs").setup()
    end
  },
  {
    "mbbill/undotree",
    event = { "BufReadPost", "BufNewFile" },
    keys = { { "<Leader>u", "<cmd>UndotreeShow<cr>" } },
  },
  {
    "tpope/vim-fugitive",
    event = { "Bufenter" },
  },
  {
    "airblade/vim-gitgutter",
    event = { "BufReadPost", "BufNewFile" },
  },
  {
    "windwp/nvim-ts-autotag",
    ft = { "html", "typescriptreact", "javascriptreact" },
    dependencies = { "nvim-treesitter/nvim-treesitter" },
    opts = {},
  },
}

require("lazy").setup(plugins, lazy_opts)
