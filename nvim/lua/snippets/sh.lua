local n = require("snippets.nodes")

return {
  n.s("#!", n.t({ "#! /usr/bin/sh", "" }), n.i(1))
}
