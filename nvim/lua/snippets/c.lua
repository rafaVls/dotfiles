local n = require("snippets.nodes")

local function get_var_name(var_text)
  local val = var_text[1][1]
  if vim.startswith(val, "int") then
    local var_name = vim.split(val, " ", true)
    return var_name[#var_name] or ""
  end
  return val or ""
end

local function is_zero_or_less(value)
  if value == "0" or vim.startswith(value, "-", true) then
    return true
  end
  return false
end

return {
  n.s("main", n.fmt([[
  int main(int argc, char *argv[]) {{
    {}
    return 0;
  }}
  ]], { n.i(1) })),
  n.s("for", n.fmt([[
  for ({} = {}; {} {} n; {}{}) {{
    {}
  }}
  ]], {
    n.c(1, { n.t("int i"), n.t("i"), n.t("int j"), n.t("j") }),
    n.i(2),
    n.f(get_var_name, { 1 }),
    n.f(function(value)
      return is_zero_or_less(value[1][1]) and "<" or ">"
    end, { 2 }),
    n.f(get_var_name, { 1 }),
    n.f(function(value)
      return is_zero_or_less(value[1][1]) and "++" or "--"
    end, { 2 }),
    n.i(3),
  })),
  n.s("while", n.fmt([[
  while ({}) {{
    {}
  }}
  ]], { n.i(1), n.i(2) })),
  n.s("do", n.fmt([[
  do {{
    {}
  }} while ({});
  ]], { n.i(1), n.i(2) })),
  n.s("str", n.fmt([[
  struct {{
    {}
  }} {};
  ]], { n.i(1, "fields"), n.i(2, "name") })),
}
