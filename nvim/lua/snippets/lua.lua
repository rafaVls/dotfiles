local n = require("snippets.nodes")

local function get_module_name(import_name)
  local modules = vim.split(import_name[1][1], ".", true)
  return modules[#modules] or ""
end

return {
  n.s(
    "req",
    n.fmt('local {} = require("{}")', { n.f(get_module_name, { 1 }), n.i(1) })
  ),
  n.s("set", n.fmt([[
  {1}.setup({{
    {2}
  }})
  ]], { n.i(1), n.i(2) })),
}
